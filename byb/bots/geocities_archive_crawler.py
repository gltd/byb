import random
import logging
from typing import List

import click
import requests
from bs4 import BeautifulSoup

from byb.core import Bot, PostContent
from byb.bots.core import platform_opt
from byb.utils import browser, image
from byb.bots.assets.geocities import SUB_NEIGHBORHOODS

log = logging.getLogger(__name__)

NAME = "geocities-archive-crawler"
BASE_URL = "http://www.oocities.org/"
DEFAULT_SUB_NEIGHBORHOOD = "Rainforest/Andes"
DEFAULT_MESSAGE = "went surfing last night: "

class GeocitiesArchiveCrawlerBot(Bot):
    def random_subneighborhood(self) -> str:
        """Generate a random geocities subneighborhood URL."""
        return BASE_URL + random.choice(SUB_NEIGHBORHOODS) + "/"

    def crawl_random_neighborhood(self) -> List[str]:
        """
        crawl all the links under a subneighborhood.
        """
        url = self.random_subneighborhood()
        while True:
            log.info(NAME + " - crawling " + url)
            r = requests.get(url)

            # check for 404
            site_text = r.text.strip().lower()
            if (
                r.status_code == 404
                or "http error 404" in site_text
                or "the page you requested was not found" in site_text
            ):
                log.warning(NAME + " could not find: " + url)
                url = self.random_subneighborhood()
                continue
            break
        log.info("successfully found a page!")
        soup = BeautifulSoup(r.content, "html.parser")
        return (
            url,
            [
                a.attrs["href"]
                for a in soup.find_all("a")
                if (
                    not a.attrs["href"].startswith("?")
                    and not a.attrs["href"].startswith("/")
                )
            ],
        )

    def generate(self, message, width, height, crop_top, **kwargs) -> PostContent:
        """generate a PostContent object from screenshot of random page."""
        base_url, page_urls = self.crawl_random_neighborhood()
        page = base_url + random.choice(page_urls)
        text = (
            message
            + page
            + """
        .
        .
        .
        .
        #bot #byb #geocitiesarchive #screenshots
        """
        )
        log.info(f"Screenshotting: {page}")
        tmpf = browser.screenshot(page, width, height)
        tmpf_jpg = tmpf.replace(".png", ".jpg")
        image.convert(tmpf, tmpf_jpg)
        bbox = image.getbbox(tmpf_jpg)
        media = image.crop(tmpf_jpg, (0, crop_top, bbox[2], bbox[3]), cleanup=True)
        return PostContent(media, text, urls=[page])


@click.command()
@click.option(
    "--message",
    default=DEFAULT_MESSAGE,
    help="A message to prefix the screenshot link, eg: " + DEFAULT_MESSAGE,
)
@click.option("--width", default=1000, help="The width of the browser")
@click.option("--height", default=768, help="The height of the browser")
@click.option("--crop_top", default=75, help="The offset to take the crop from")
@platform_opt
def cli(**kwargs):
    """
    Take a screenshot of a random page from geocities archive.
    """
    GeocitiesArchiveCrawlerBot().post_content_to_platforms(**kwargs)
